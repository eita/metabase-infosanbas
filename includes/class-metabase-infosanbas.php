<?php
/**
 * Main plugin class file.
 *
 * @package WordPress Plugin Template/Includes
 */

if (! defined('ABSPATH') ) {
    exit;
}

/**
 * Main plugin class.
 */
class Metabase_Infosanbas
{

    /**
     * The single instance of Metabase_Infosanbas.
     *
     * @var    object
     * @access private
     * @since  1.0.0
     */
	private static $_instance = null; //phpcs:ignore

    /**
     * Local instance of Metabase_Infosanbas_Admin_API
     *
     * @var Metabase_Infosanbas_Admin_API|null
     */
    public $admin = null;

    /**
     * Settings class object
     *
     * @var    object
     * @access public
     * @since  1.0.0
     */
    public $settings = null;

    /**
     * The version number.
     *
     * @var    string
     * @access public
     * @since  1.0.0
     */
	public $_version; //phpcs:ignore

    /**
     * The token.
     *
     * @var    string
     * @access public
     * @since  1.0.0
     */
	public $_token; //phpcs:ignore

    /**
     * The main plugin file.
     *
     * @var    string
     * @access public
     * @since  1.0.0
     */
    public $file;

    /**
     * The main plugin directory.
     *
     * @var    string
     * @access public
     * @since  1.0.0
     */
    public $dir;

    /**
     * The plugin assets directory.
     *
     * @var    string
     * @access public
     * @since  1.0.0
     */
    public $assets_dir;

    /**
     * The plugin assets URL.
     *
     * @var    string
     * @access public
     * @since  1.0.0
     */
    public $assets_url;

    /**
     * Suffix for JavaScripts.
     *
     * @var    string
     * @access public
     * @since  1.0.0
     */
    public $script_suffix;

    /**
     * Constructor funtion.
     *
     * @param string $file    File constructor.
     * @param string $version Plugin version.
     */
    public function __construct( $file = '', $version = '1.0.0' )
    {
        $this->_version = $version;
        $this->_token   = 'metabase_infosanbas';

        // Load plugin environment variables.
        $this->file       = $file;
        $this->dir        = dirname($this->file);
        $this->assets_dir = trailingslashit($this->dir) . 'assets';
        $this->assets_url = esc_url(trailingslashit(plugins_url('/assets/', $this->file)));

        $this->script_suffix = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? '' : '.min';

        register_activation_hook($this->file, array( $this, 'install' ));

        // Load frontend JS & CSS.
        add_action('wp_enqueue_scripts', array( $this, 'enqueue_styles' ), 10);
        add_action('wp_enqueue_scripts', array( $this, 'enqueue_scripts' ), 10);

        // Load admin JS & CSS.
        add_action('admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ), 10, 1);
        add_action('admin_enqueue_scripts', array( $this, 'admin_enqueue_styles' ), 10, 1);

        // Load API for generic admin functions.
        if (is_admin() ) {
            $this->admin = new Metabase_Infosanbas_Admin_API();
        }

        // Handle localisation.
        $this->load_plugin_textdomain();
        add_action('init', array( $this, 'load_localisation' ), 0);

        // register shortcodes
        add_shortcode('mbimap', array( $this, 'metabase_infosanbas_map'));
        add_shortcode('mbiframe', array( $this, 'metabase_infosanbas_iframe'));
        add_shortcode('mbibar', array( $this, 'metabase_infosanbas_bar'));
        add_shortcode('mbitable', array( $this, 'metabase_infosanbas_table'));
        add_shortcode('mbimunicipio', array( $this, 'metabase_infosanbas_municipio'));
        add_shortcode('mbiatributo', array( $this, 'metabase_infosanbas_atributo'));
        add_shortcode('mbinumber', array( $this, 'metabase_infosanbas_number'));
        add_shortcode('mbimapsingle', array( $this, 'metabase_infosanbas_mapsingle'));

        add_shortcode('mbi_modal', array( $this, 'metabase_infosanbas_modal'));
    } // End __construct ()

    /**
     * Implements MBIatributo shortcode
     *
     * @param array $atts Shortcode attributes
     *
     * @return string
     */
    public function metabase_infosanbas_atributo( $atts )
    {

        global $ATRIBUTOS, $INDICADORES;

        if (array_key_exists('atributo', $atts)) {
            $atributo = $atts['atributo'];
        } else {
            return __('atributo is a required attribute for this shortcode', 'metabase-infosanbas');
        }
        if (array_key_exists('servico', $atts)) {
            $servico = $atts['servico'];
        } else {
            return __('servico is a required attribute for this shortcode', 'metabase-infosanbas');
        }

        $out = "
			<p>
				". $ATRIBUTOS[$servico][$atributo][0] . "
			</p>
		";

        if (( $atributo == 'Igualdade e não discriminação') && ($servico == 'Esgoto')) {
            // $out .= "
            // <p style='overflow:auto'>O ÍNDICE DE IGUALDADE E NÃO DISCRIMINAÇÃO foi calculado a partir de um índice de desigualdade. O índice de desigualdade foi calculado pela soma das razões entre o grupo menos vulnerável e o grupo mais vulnerável. Como foram 3 critérios (Cor/Raça, Nível de instrução e Renda Domiciliar Per Capita), o resultado foi dividido por 3. Em uma condição de total igualdade, o resultado deveria ser 1. O índice de IGUALDADE é o índice de desigualdade reescalonado de 0 a 1 e invertido, ou seja, quanto maior o valor (mais próximo de 1) mais igualitário é o município; quanto menor o valor (mais próximo de 0), maior a desigualdade.</p>";

            $texto = "
			<p>O <b>Índice de Igualdade e Não Discriminação</b> é calculado a partir da desagregação de três variáveis do Censo Demográfico: “renda domiciliar per capita”, “nível de escolaridade” e “cor da pele” (ou raça), que deram origem aos indicadores da fórmula de cálculo abaixo.</p>

			<p>O cálculo deste <b>Índice de Igualdade e Não Discriminação</b> foi feito a partir de um índice de <em>desigualdade</em>, gerado pelos três critérios acima (renda domiciliar per capita; nível de escolaridade; cor da pele ou raça). A base de cálculo deste índice de desigualdade é a razão entre a proporção de acesso a serviços “pelo menos básicos” de esgotamento sanitário por subgrupos populacionais mais vulneráveis à privação desses serviços, e subgrupos menos vulneráveis à essa privação, conforme mostrado na equação abaixo:</p>
			<p><img src='https://infosanbas.org.br/wp-content/uploads/2020/07/ql_b810846165f0dd3590ef6a370c58fb4b_l3.png' /></p>
			<p>Onde:<br>
			ID = Índice de desigualdade<br>
			B = Proporção da população branca com acesso a serviços “pelo menos básicos” de esgoto<br>
			N = Proporção da população não branca com acesso a serviços “pelo menos básicos” de esgoto<br>
			SC = Proporção da população com ensino superior completo com acesso a serviços “pelo menos<br> básicos” de esgoto<br>
			SI = Proporção da população sem instrução ou com ensino fundamental incompleto com acesso a serviços “pelo menos básicos” de esgoto<br>
			LR = Proporção da população residente em domicílios com renda domiciliar per capita igual ou superior a 1 salário mínimo com acesso a serviços “pelo menos básicos” de esgoto<br>
			LP = Proporção da população residente em domicílios com renda domiciliar per capita abaixo da linha de pobreza (inferior a 1/4 de salário mínimo) com acesso a serviços “pelo menos básicos” de esgoto</p>

			<p>O <b>Índice de Igualdade e Não Discriminação</b> é o índice de desigualdade reescalonado de 0 a 1 e invertido, de forma que quanto maior o seu valor (quanto mais próximo de 1) mais igualitário é o município; quanto menor o valor (mais próximo de 0), maior a desigualdade.</p>";

            $modal = "
				<button id='legendabtn_ig$servico' data-legenda='ig$servico' class='glossario'>?</button>
				<div id='legendamodal_ig$servico' class='modal'>
					<div class='modal-content'>
						<span class='close'>&times;</span>
						<p>" . $texto . "</p>
					</div>
				</div>
			";

            $out .= "
			<p>O Índice de Igualdade e Não Discriminação é calculado a partir de 6 indicadores:</p>

			<ul>
				<li>Proporção da população branca com acesso a serviços “pelo menos básicos” de esgoto</li>
				<li>Proporção da população não branca com acesso a serviços “pelo menos básicos” de esgoto</li>
				<li>Proporção da população com ensino superior completo com acesso a serviços “pelo menos básicos” de esgoto</li>
				<li>Proporção da população sem instrução ou com ensino fundamental incompleto com acesso a serviços “pelo menos básicos” de esgoto</li>
				<li>Proporção da população residente em domicílios com renda domiciliar per capita igual ou superior a 1 salário mínimo com acesso a serviços “pelo menos básicos” de esgoto</li>
				<li>Proporção da população residente em domicílios com renda domiciliar per capita abaixo da linha de pobreza (inferior a 1/4 de salário mínimo) com acesso a serviços “pelo menos básicos” de esgoto</li>
			</ul>

			<p>O cálculo foi feito a partir de um índice de desigualdade, gerado pelos critérios acima (renda domiciliar per capita; nível de escolaridade; cor da pele ou raça) que foi invertido, de forma que, quanto maior seu valor, mais igualitário é o município; quanto menor o valor maior a desigualdade.$modal</p>";


        } else {

            if (( $atributo == 'Acessibilidade Física') && ($servico == 'Água')) {
                $textoadd = ", que são os mesmos do atributo “Disponibilidade”";
            } else {
                $textoadd = "";
            }

            $atributo_principio = !(( $atributo == 'Igualdade e não discriminação') && ($servico == 'Água')) ? "atributo" : "princípio de";

            $out .= "
				<p>
					O $atributo_principio $atributo é calculado a partir de " . count($ATRIBUTOS[$servico][$atributo][1]) . " indicadores$textoadd:
				<ul>
			";

            foreach ($ATRIBUTOS[$servico][$atributo][1] as $indicador) {
                $out .= "
					<li>" . $INDICADORES[$servico][$indicador][0] . ". Fonte: " . $INDICADORES[$servico][$indicador][1] . "
				";

                // modal
                if ($INDICADORES[$servico][$indicador][3] != "") {

                    $out .= "
						<button id='legendabtn_$indicador$servico' data-legenda='$indicador$servico' class='glossario'>?</button>
					";
                    $out .= "
						<div id='legendamodal_$indicador$servico' class='modal'>
						  <div class='modal-content'>
						    <span class='close'>&times;</span>
						    <p>" . $INDICADORES[$servico][$indicador][3] . "</p>
						  </div>
						</div>
					";
                }

                $out .= "
				 </li>
				 ";
            }

            $out .= "
				</ul>
				</p>";

            if ($servico == 'Água' && $atributo == 'Disponibilidade') {
                $out .= "";
            }

            if ($servico == 'Água' && $atributo == 'Acessibilidade Física') {
                $out .= "";
            }

            if ($servico == 'Água' && $atributo == 'Igualdade e não discriminação') {
                $texto_modal = "
					<p>O Índice Igualdade e Não Discriminação foi calculado por um método estatístico denominado Análise de Componentes Principais (procedimento de Análise Variada), pelo qual a informação contida nos indicadores é sintetizada em um conjunto menor de elementos – os componentes principais – a partir do qual o índice é calculado.</p>

					<p>No período mais antigo, o primeiro componente principal explicou 48,5% da variância dos dados municipais e, no período mais recente, explicou 47% da variabilidade. Em ambos os períodos, os indicadores com maior peso nesse componente foram os que dizem respeito ao acesso de sub subgrupos populacionais à rede geral de distribuição de água (população rural; população acima de 25 anos de idade sem instrução ou com ensino fundamental incompleto; população com renda domiciliar ‘per capita’ abaixo da linha de pobreza; população de cor ou raça não branca) e o indicador referente à “diferença entre as proporções da população com pelo menos ensino fundamental completo e da população com ensino fundamental incompleto (acima de 25 anos de idade) no acesso à rede geral de distribuição de água. </p>
					";
                $modal = "
						<button id='legendabtn_ig$servico' data-legenda='ig$servico' class='glossario'>?</button>
						<div id='legendamodal_ig$servico' class='modal'>
							<div class='modal-content'>
								<span class='close'>&times;</span>
								<p>" . $texto_modal . "</p>
							</div>
						</div>
					";
                $texto = "<p>Este índice foi calculado por um método estatístico denominado “Análise de Componentes Principais”. Em ambos os períodos, os indicadores com maior peso foram os que se referem ao acesso de sub subgrupos populacionais à rede geral de distribuição de água, e o indicador referente à “diferença entre as proporções da população com pelo menos ensino fundamental completo e da população com ensino fundamental incompleto no acesso à rede geral de distribuição de água”.$modal</p>";

                $out .= $texto;
            }


        }

        // não consegui fazer a inclusão correta do arquivo frontend.js, portanto o modal ficou aqui. O correto era estar no frontend.js
        $modal_script = '
		<script>
		jQuery( document ).ready(
			function ($) {
				$("[id^=legendabtn]").click(function(){

					var indicador = $(this).attr("data-legenda")
					var modal = $("#legendamodal_"+indicador)
					modal.css("display", "block");

					$(".modal-content span").click(function(e){
						modal.css("display", "none");
					});

					window.onclick = function(event) {
					  if (modal.is(event.target)) {
					    modal.css("display", "none");
					  }
					}
				});
			}
		);
		</script>
		';

        return $out . $modal_script;
    }

    /**
     * Implements MBItable shortcode
     *
     * @param array $atts Shortcode attributes
     *
     * @return string
     */
    public function metabase_infosanbas_number( $atts )
    {

        if (array_key_exists('question', $atts)) {
            $question = $atts['question'];
        } else {
            return __('Question is a required attribute for this shortcode', 'metabase-infosanbas');
        }
        if (array_key_exists('ano', $atts)) {
            $ano = $atts['ano'];
        } else {
            return __('Ano is a required attribute for this shortcode', 'metabase-infosanbas');
        }
        if (array_key_exists('municipio', $atts)) {
            $municipio = $atts['municipio'];
        } else {
            return __('Municipio is a required attribute for this shortcode', 'metabase-infosanbas');
        }
        if (array_key_exists('municipio', $atts)) {
            $attname = $atts['attname'];
        } else {
            return __('attname is a required attribute for this shortcode', 'metabase-infosanbas');
        }

        $parameters = array(
        'municipio' => $municipio,
        'ano' => $ano,
        );
        $data = $this->metabase_get_answer($question, $parameters);
        return $data[0]->$attname;
    }

    /**
     * Implements MBItable shortcode
     *
     * @param array $atts Shortcode attributes
     *
     * @return string
     */
    public function metabase_infosanbas_municipio( $atts )
    {

        global $ATRIBUTOS, $INDICADORES;

        if (array_key_exists('question', $atts)) {
            $question = $atts['question'];
        } else {
            return __('Question is a required attribute for this shortcode', 'metabase-infosanbas');
        }
        if (array_key_exists('servico', $atts)) {
            $servico = $atts['servico'];
        } else {
            return __('Serviço is a required attribute for this shortcode', 'metabase-infosanbas');
        }
        if (array_key_exists('municipio', $atts)) {
            $municipio = $atts['municipio'];
        } else {
            return __('Municipio is a required attribute for this shortcode', 'metabase-infosanbas');
        }
        if (array_key_exists('ranking', $atts)) {
            $ranking = $atts['ranking'];
        } else {
            return __('ranking is a required attribute for this shortcode', 'metabase-infosanbas');
        }

        $parameters = array(
        'municipio' => $municipio,
        );
        $data = $this->metabase_get_answer($question, $parameters);

        $output = "";
        foreach ($ATRIBUTOS[$servico] as $atributo => $indicadores_atributo) {

            $ranking_parameters = array(
            'servico' => $servico,
            'atributo' => $atributo,
            );
            $ranking_data = $this->metabase_get_answer($ranking, $ranking_parameters);

            usort(
                $ranking_data, function ($a, $b) {
                    return ($a->{"Índice (antigo)"} < $b->{"Índice (antigo)"});
                }
            );

            $i = 1; $j = 1;
            foreach ($ranking_data as $mun) {
                if (($i > 1) && ($ranking_data[$i-1]->{"Índice (antigo)"} != $ranking_data[$i-2]->{"Índice (antigo)"})) {
                    $j = $i;
                }
                if($mun->{"Município"} == $municipio) {
                    $posicao_antiga = $j;
                    break;
                }
                $i++;
            }

            usort(
                $ranking_data, function ($a, $b) {
                    return ($a->{"Índice (recente)"} < $b->{"Índice (recente)"});
                }
            );

            $i = 1; $j = 1;
            foreach ($ranking_data as $mun) {
                if (($i > 1) && ($ranking_data[$i-1]->{"Índice (recente)"} != $ranking_data[$i-2]->{"Índice (recente)"})) {
                    $j = $i;
                }
                if($mun->{"Município"} == $municipio) {
                    $posicao_recente = $j;
                    break;
                }
                $i++;
            }


            $n_indicadores = count($indicadores_atributo[1]);

            $output .= "
					<h3><span style='color: #333333;'>$atributo</span></h3>
				";

            $output .= "
					<p style='overflow:auto'>$indicadores_atributo[0]</p>
				";

            $output .= "
			<p style='overflow:auto'>
				<img class='wp-image-62904 alignleft' src='/wp-content/uploads/2019/06/posicao.png' alt='' width='26' height='39' /> No período mais antigo, $municipio ficou na <b>".$posicao_antiga."ª posição</b> entre os 34 municípios da RMBH. No período mais recente, ficou na <b>".$posicao_recente."ª posição</b>.
			</p>
			";

            if (( $atributo == 'Igualdade e não discriminação') && ($servico == 'Esgoto')) {
                // $output .= "
                // <p style='overflow:auto'>O ÍNDICE DE IGUALDADE E NÃO DISCRIMINAÇÃO foi calculado a partir de um índice de desigualdade. O índice de desigualdade foi calculado pela soma das razões entre o grupo menos vulnerável e o grupo mais vulnerável. Como foram 3 critérios (Cor/Raça, Nível de instrução e Renda Domiciliar Per Capita), o resultado foi dividido por 3. Em uma condição de total igualdade, o resultado deveria ser 1. O índice de IGUALDADE é o índice de desigualdade reescalonado de 0 a 1 e invertido, ou seja, quanto maior o valor (mais próximo de 1) mais igualitário é o município; quanto menor o valor (mais próximo de 0), maior a desigualdade.</p>
                // ";

                $texto = "
				<p>O <b>Índice de Igualdade e Não Discriminação</b> é calculado a partir da desagregação de três variáveis do Censo Demográfico: “renda domiciliar per capita”, “nível de escolaridade” e “cor da pele” (ou raça), que deram origem aos indicadores da fórmula de cálculo abaixo.</p>

				<p>O cálculo deste <b>Índice de Igualdade e Não Discriminação</b> foi feito a partir de um índice de <em>desigualdade</em>, gerado pelos três critérios acima (renda domiciliar per capita; nível de escolaridade; cor da pele ou raça). A base de cálculo deste índice de desigualdade é a razão entre a proporção de acesso a serviços “pelo menos básicos” de esgotamento sanitário por subgrupos populacionais mais vulneráveis à privação desses serviços, e subgrupos menos vulneráveis à essa privação, conforme mostrado na equação abaixo:</p>
				<p><img src='https://infosanbas.org.br/wp-content/uploads/2020/07/ql_b810846165f0dd3590ef6a370c58fb4b_l3.png' /></p>
				<p>Onde:<br>
				ID = Índice de desigualdade<br>
				B = Proporção da população branca com acesso a serviços “pelo menos básicos” de esgoto<br>
				N = Proporção da população não branca com acesso a serviços “pelo menos básicos” de esgoto<br>
				SC = Proporção da população com ensino superior completo com acesso a serviços “pelo menos<br> básicos” de esgoto<br>
				SI = Proporção da população sem instrução ou com ensino fundamental incompleto com acesso a serviços “pelo menos básicos” de esgoto<br>
				LR = Proporção da população residente em domicílios com renda domiciliar per capita igual ou superior a 1 salário mínimo com acesso a serviços “pelo menos básicos” de esgoto<br>
				LP = Proporção da população residente em domicílios com renda domiciliar per capita abaixo da linha de pobreza (inferior a 1/4 de salário mínimo) com acesso a serviços “pelo menos básicos” de esgoto</p>

				<p>O <b>Índice de Igualdade e Não Discriminação</b> é o índice de desigualdade reescalonado de 0 a 1 e invertido, de forma que quanto maior o seu valor (quanto mais próximo de 1) mais igualitário é o município; quanto menor o valor (mais próximo de 0), maior a desigualdade.</p>";

                $modal = "
					<button id='legendabtn_ig$servico' data-legenda='ig$servico' class='glossario'>?</button>
					<div id='legendamodal_ig$servico' class='modal'>
						<div class='modal-content'>
							<span class='close'>&times;</span>
							<p>" . $texto . "</p>
						</div>
					</div>
				";

                $output .= "
				<p>O “Índice de Igualdade e Não Discriminação” foi calculado a partir de três critérios: cor/raça, nível de instrução e renda domiciliar <em>per capita</em>. Quanto maior o valor (mais próximo de 1) mais igualitário é o município; quanto menor o valor (mais próximo de 0), maior a desigualdade.$modal</p>";



                foreach ($ranking_data as $mun) {
                    if($mun->{"Município"} == $municipio) {
                        $antigo = $mun->{"Índice (antigo)"};
                        $recente = $mun->{"Índice (recente)"};
                        break;
                    }
                }

            } else {

                $atributo_principio = !(( $atributo == 'Igualdade e não discriminação') && ($servico == 'Água')) ? "atributo" : "princípio de";


                $output .= "
				<p style='overflow:auto'>O $atributo_principio $atributo é calculado a partir de <b>$n_indicadores indicadores</b>:</p>
				";
            }



            foreach ($indicadores_atributo[1] as $indicador) {
                $indice_antigo = "indice" . $indicador . "_1";
                $indice_recente = "indice" . $indicador . "_2";
                $valor_antigo = $data[0]->$indice_antigo;
                $valor_recente = $data[0]->$indice_recente;

                if(($indicador == 4) and ($servico == 'Água')) {
                    $valor_antigo *= 100;
                    $valor_recente *= 100;
                }

                $output .= "
					<h5><span style='color: #662483;'>".$INDICADORES[$servico][$indicador][0]."</span>
				";

                // modal
                if ($INDICADORES[$servico][$indicador][3] != "") {

                    $output .= "
						<button id='legendabtn_$indicador$servico' data-legenda='$indicador$servico' class='glossario'>?</button>
					";
                    $output .= "
						<div id='legendamodal_$indicador$servico' class='modal'>
						  <div class='modal-content'>
						    <span class='close'>&times;</span>
						    <p>" . $INDICADORES[$servico][$indicador][3] . "</p>
						  </div>
						</div>
					";
                }

                $output .= "
					</h5><p style='overflow:auto'>
				";

                if ($INDICADORES[$servico][$indicador][2] == 'BOOLEANO') {
                    $valor_recente = $valor_recente ? 'positivo' : 'negativo';
                    $valor_antigo = $valor_antigo ? 'positivo' : 'negativo';
                    if ($valor_recente == 'positivo') {
                        $res = 'bom';
                    } else {
                        $res = 'ruim';
                    }
                } else {
                    if((( $valor_antigo < $valor_recente ) && ($INDICADORES[$servico][$indicador][2] == 'MAIOR')) || (( $valor_antigo > $valor_recente ) && ($INDICADORES[$servico][$indicador][2] == 'MENOR'))) {
                        $res = 'melhorou';
                    } elseif((( $valor_antigo > $valor_recente ) && ($INDICADORES[$servico][$indicador][2] == 'MAIOR')) || (( $valor_antigo < $valor_recente ) && ($INDICADORES[$servico][$indicador][2] == 'MENOR'))) {
                        $res = 'piorou';
                    } else {
                        $res = 'estavel';
                    }
                    $valor_recente = number_format($valor_recente, 2, ",", ".") . $INDICADORES[$servico][$indicador][4];
                    $valor_antigo = number_format($valor_antigo, 2, ",", ".") . $INDICADORES[$servico][$indicador][4];
                }

                if($res == 'piorou' ) {
                    $output .= "
						<img class='alignone size-full wp-image-62899 alignleft' src='/wp-content/uploads/2019/06/piorou.png' alt='' width='42' height='41' />Neste indicador, $municipio <strong>piorou</strong>, mudando de  $valor_antigo para $valor_recente. Fonte: ". $INDICADORES[$servico][$indicador][1]
                    ;
                } elseif ($res == 'melhorou' ) {
                    $output .= "
						<img class='alignone size-full wp-image-62899 alignleft' src='/wp-content/uploads/2019/06/melhorou.png' alt='' width='42' height='41' />Neste indicador, $municipio <strong>melhorou</strong>, mudando de $valor_antigo para $valor_recente. Fonte: ". $INDICADORES[$servico][$indicador][1]
                    ;
                } elseif ($res == 'estavel' ) {
                    $output .= "
						<img class='alignone size-full wp-image-62899 alignleft' src='/wp-content/uploads/2019/06/estavel.png' alt='' width='42' height='41' />Neste indicador, $municipio <strong>ficou estável</strong> no valor $valor_antigo. Fonte: ". $INDICADORES[$servico][$indicador][1]
                    ;
                } elseif ($res == 'bom' ) {
                    $output .= "
						<img class='alignone size-full wp-image-62899 alignleft' src='/wp-content/uploads/2019/06/melhorou.png' alt='' width='42' height='41' />Neste indicador, $municipio tem resultado <strong>$res</strong>, com resultado <strong>$valor_antigo</strong> no tempo antigo, e resultado <strong>$valor_recente</strong> no tempo recente. Fonte: ". $INDICADORES[$servico][$indicador][1]
                    ;
                } elseif ($res == 'ruim' ) {
                    $output .= "
						<img class='alignone size-full wp-image-62899 alignleft' src='https://infosanbas.org.br/wp-content/uploads/2019/06/piorou.png' alt='' width='42' height='41' />Neste indicador, $municipio tem resultado <strong>$res</strong>, com resultado <strong>$valor_antigo</strong> no tempo antigo, e resultado <strong>$valor_recente</strong> no tempo recente. Fonte: ". $INDICADORES[$servico][$indicador][1]
                    ;
                }
                $output .= "
					</p>
				";
            }
            $output .= "
				<hr />
			";
        }

        // não consegui fazer a inclusão correta do arquivo frontend.js, portanto o modal ficou aqui. O correto era estar no frontend.js
        $modal_script = '
		<script>
		jQuery( document ).ready(
			function ($) {
				$("[id^=legendabtn]").click(function(){

					var indicador = $(this).attr("data-legenda")
					var modal = $("#legendamodal_"+indicador)
					modal.css("display", "block");

					$(".modal-content span").click(function(e){
						modal.css("display", "none");
					});

					window.onclick = function(event) {
					  if (modal.is(event.target)) {
					    modal.css("display", "none");
					  }
					}
				});
			}
		);
		</script>
		';

        return $output . $modal_script;

    }

    /**
     * Implements MBItable shortcode
     *
     * @param array $atts Shortcode attributes
     *
     * @return string
     */
    public function metabase_infosanbas_table( $atts )
    {
        if (array_key_exists('question', $atts)) {
            $question = $atts['question'];
        } else {
            return __('Question is a required attribute for this shortcode', 'metabase-infosanbas');
        }

        if (array_key_exists('atributo', $atts)) {
            $atributo = $atts['atributo'];
        } else {
            return __('Atributo is a required attribute for this shortcode', 'metabase-infosanbas');
        }

        if (array_key_exists('servico', $atts)) {
            $servico = $atts['servico'];
        } else {
            return __('Servico is a required attribute for this shortcode', 'metabase-infosanbas');
        }

        $rand = rand();
        $parameters = array(
        'atributo' => $atributo,
        'servico' => $servico,
        );

        $data = $this->metabase_get_answer($question, $parameters);
        // ranking antigo
        usort(
            $data, function ($a, $b) {
                return ($a->{"Índice (antigo)"} < $b->{"Índice (antigo)"});
            }
        );

        $i = 1; $j = 1;
        foreach ($data as $index => $mun) {
            if (($i > 1) && ($data[$i-1]->{"Índice (antigo)"} != $data[$i-2]->{"Índice (antigo)"})) {
                $j = $i;
            }
            $data[$index]->{"Ranking (antigo)"} = $j;
            $i++;
        }

        // ranking recente
        usort(
            $data, function ($a, $b) {
                return ($a->{"Índice (recente)"} < $b->{"Índice (recente)"});
            }
        );

        $i = 1; $j = 1;
        foreach ($data as $index => $mun) {
            if (($i > 1) && ($data[$i-1]->{"Índice (recente)"} != $data[$i-2]->{"Índice (recente)"})) {
                $j = $i;
            }
            $data[$index]->{"Ranking (recente)"} = $j;
            $i++;
        }

        // start building the table
        $table = "
			<table class=display style='width:500px; height:800px' id=table-$rand>
				<thead>
					<tr>
		";
        foreach ($data as $row) {
            unset($row->{"Índice (antigo)"});
            unset($row->{"Índice (recente)"});
        }
        $headers = get_object_vars($data[0]);
        foreach ($headers as $key => $value) {
            $table .= "
						<th>$key</th>
			";
        }
        $table .= "
				</tr>
			</thead>
			<tbody>
		";

        foreach ($data as $row) {
            $row = get_object_vars($row);
            $table .= "
				<tr>
			";
            foreach ($row as $column_key => $column_value) {
                if ($column_key != "Município") {
                    $class = "class='position'";
                } else {
                    $class = "";
                }
                $table .= "
					 <td $class>$column_value</td>
				";
            }
            $table .= "
				</tr>
			";
        }
        $table .= "
				</tbody>
			</table>
		";

        $js = "
			jQuery('#table-$rand').DataTable({
				'paging': false,
				'searching': false,
				'order': [[ 2, 'asc' ]]
			});
		";

        return $table . "<script>" . $js . "</script>";

    }

    /**
     * Implements MBIbar shortcode
     *
     * @param array $atts Shortcode attributes
     *
     * @return string
     */
    public function metabase_infosanbas_bar( $atts )
    {

        global $ATRIBUTOS;

        if (array_key_exists('question', $atts)) {
            $question = $atts['question'];
        } else {
            return __('Question is a required attribute for this shortcode', 'metabase-infosanbas');
        }
        if (array_key_exists('municipio', $atts)) {
            $municipio = $atts['municipio'];
        } else {
            return __('Município is a required attribute for this shortcode', 'metabase-infosanbas');
        }
        if (array_key_exists('servico', $atts)) {
            $servico = $atts['servico'];
        } else {
            return __('Servico is a required attribute for this shortcode', 'metabase-infosanbas');
        }

        $rand = rand();

        $antigo = null;
        $recente = null;
        $atributos =  null;
        foreach ($ATRIBUTOS[$servico] as $atributo => $values) {
            $atributos[] = $atributo;
        }

        foreach ($atributos as $atributo) {
            $parameters = array(
            'atributo' => $atributo,
            'servico' => $servico,
            'municipio' => $municipio
            );
            $data = $this->metabase_get_answer($question, $parameters);
            $antigo[] = $data[0]->{"Classificação (antigo)"};
            $recente[] = $data[0]->{"Classificação (recente)"};
        }


        $js = "
		var chart = c3.generate({
				bindto: '#chart-$rand',
		    data: {
		        columns: [
		            ['Antigo', " . implode(", ", $antigo) . "],
		            ['Recente', " . implode(", ", $recente) . "]
		        ],
						colors: {
							'Antigo': '#906D36',
							'Recente': '#662483'
						},
		        type: 'bar'
		    },
				axis: {
		        x: {
		            type: 'category',
		            categories: ['" . implode("', '", $atributos) . "']
		        },
						y: {
							max: 4,
              min: 1,
							tick: {
								count: 5,
								values: [0,1,2,3,4],
              	format: function(value) {
									switch (value){
										case 1:
											return 'Muito abaixo da média';
											break;
										case 2:
											return 'Abaixo da média';
											break;
										case 3:
											return 'Acima da média';
											break;
										case 4:
											return 'Muito acima da média';
											break;
									}
	            	}
							}
						}
		    },
		    bar: {
		        width: {
		            ratio: 0.5 // this makes bar width 50% of length between ticks
		        }
		        // or
		        //width: 100 // this makes bar width 100px
		    }
		});

		";
        // this should be the right way to do it, but doesn't work in ajax themes
        // wp_add_inline_script('metabase_infosanbas-frontend', $js, 'after');

        return "<div style='width:100%; height:250px' id=chart-$rand></div><script>$js</script>";
    }

    /**
     * Implements MBIframe shortcode
     *
     * @param array $atts Shortcode attributes
     *
     * @return string
     */
    public function metabase_infosanbas_iframe( $atts )
    {
        if (array_key_exists('question', $atts)) {
            $question = $atts['question'];
        } else {
            return __('Question is a required attribute for this shortcode', 'metabase-infosanbas');
        }

        $width = array_key_exists('width', $atts) ? $atts['width'] : 800;
        $height = array_key_exists('height', $atts) ? $atts['height'] : 600;
        $style = array_key_exists('style', $atts) ? $atts['style'] : "";

        $iframe = '
	    <iframe
	        src="' . get_option('mbi_metabase_url') . '/public/question/' . $question . '"
	        frameborder="0"
	        width="' . $width . '"
	        height="' . $height . '"
	        allowtransparency
	        style="' . $style . '"
	    ></iframe>
	  ';

        return $iframe;
    }

    /**
     * Implements MBImapsingle shortcode
     *
     * @param array $atts Shortcode attributes
     *
     * @return string
     */
    function metabase_infosanbas_mapsingle( $atts )
    {

        if (array_key_exists('map', $atts)) {
            $map = $atts['map'];
        } else {
            return __('Map is a required attribute for this shortcode', 'metabase-infosanbas');
        }
        if (array_key_exists('municipio', $atts)) {
            $municipio = $atts['municipio'];
        } else {
            return __('municipio is a required attribute for this shortcode', 'metabase-infosanbas');
        }

        $question = array_key_exists('question', $atts) ? $atts['question'] : "";
        $map_key = array_key_exists('map_key', $atts) ? $atts['map_key'] : "";
        $question_key = array_key_exists('question_key', $atts) ? $atts['question_key'] : "";
        $question_value = array_key_exists('question_value', $atts) ? $atts['question_value'] : "";

        $servico = array_key_exists('servico', $atts) ? $atts['servico'] : null;
        $atributo = array_key_exists('atributo', $atts) ? $atts['atributo'] : null;

        $parameters = array(
        'municipio' => $municipio,
        );

        $width = array_key_exists('width', $atts) ? $atts['width'] : "";
        $height = array_key_exists('height', $atts) ? $atts['height'] : "";

        $rand = rand(); // this is used to differ two maps in the same page

        $coordinates = $this->metabase_get_answer($map, $parameters);

        $js = "
			var geojsonFeature = [
		";
        foreach ($coordinates as $coordinate) {
            $js .= $coordinate->shape . ",
			";
        }
        $js .= "]";

        $js .= "

			var mymap$rand = L.map('map-$rand',{
				scrollWheelZoom:false,
				zoomControl: false
			});

			L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
				maxZoom: 18,
				attribution: 'Map data &copy; <a href=\"http://openstreetmap.org\">OpenStreetMap</a> contributors, ' +
					'<a href=\"http://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>, ' +
					'Imagery © <a href=\"http://mapbox.com\">Mapbox</a>',
				id: 'mapbox.streets'
			}).addTo(mymap$rand);

			L.control.zoom({
					position: 'topright'
			}).addTo(mymap$rand);

			var geojson = L.geoJSON(geojsonFeature).addTo(mymap$rand);
			mymap$rand.fitBounds(L.geoJSON(geojsonFeature).getBounds());
		";


        if ($height && $width) {
            $style = "style='height:$height; width:$width'";
        } else {
            $style = "";
        }

        return ( "<div $style class='map' id='map-$rand'></div><script>$js</script>" );

    }

    /**
     * Implements MBImap shortcode
     *
     * @param array $atts Shortcode attributes
     *
     * @return string
     */
    function metabase_infosanbas_map( $atts )
    {

        if (array_key_exists('map', $atts)) {
            $map = $atts['map'];
        } else {
            return __('Map is a required attribute for this shortcode', 'metabase-infosanbas');
        }

        $question = array_key_exists('question', $atts) ? $atts['question'] : "";
        $map_key = array_key_exists('map_key', $atts) ? $atts['map_key'] : "";
        $question_key = array_key_exists('question_key', $atts) ? $atts['question_key'] : "";
        $question_value = array_key_exists('question_value', $atts) ? $atts['question_value'] : "";

        $servico = array_key_exists('servico', $atts) ? $atts['servico'] : null;
        $atributo = array_key_exists('atributo', $atts) ? $atts['atributo'] : null;
        if ($servico AND $atributo) {
            $parameters = array(
            'servico' => $servico,
            'atributo' => $atributo,
            );
        } else {
            $parameters = null;
        }

        // $infobox = array_key_exists('infobox', $atts) ? $atts['infobox'] : "";
        // $infobox = explode("#", $infobox);

        $width = array_key_exists('width', $atts) ? $atts['width'] : "";
        $height = array_key_exists('height', $atts) ? $atts['height'] : "";

        $rand = rand(); // this is used to differ two maps in the same page
        if ($question) {
            $data = $this->metabase_get_answer($question, $parameters);
        } else {
            $data = null;
        }
        $coordinates = $this->metabase_get_answer($map);

        $js = "var geojsonFeature = [";
        foreach ($coordinates as $coordinate) {
            $coordinate_dec = json_decode($coordinate->Shape);

            if ($data) {
                $mk = $coordinate_dec->properties->$map_key;
                foreach ($data as $data_row) {
                    if ($data_row->$question_key == $mk) {
                            $coordinate_dec->properties->category = $data_row->$question_value;
                            // foreach ($infobox as $infobox_att) {
                            //     $coordinate_dec->properties->$infobox_att = $data_row->$infobox_att;
                            // }
                    }
                }
            }

            $coordinate->shape = json_encode($coordinate_dec);

            $js .= $coordinate->shape . ",
				";
        }
        $js .= "]";

            $js .= "

					function highlightFeature(e) {
						var layer = e.target;

						layer.setStyle({
							fillColor: 'blue',
							weight: 2,
							opacity: .5,
		        	color: 'white',  //Outline color
	        		fillOpacity: 0.5
						});

						if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
							layer.bringToFront();
						}
						info$rand.update(layer.feature.properties);
					}

					function resetHighlight(e) {
						geojson.resetStyle(e.target);
						info$rand.update();
					}

					function zoomToFeature(e) {
						mymap$rand.fitBounds(e.target.getBounds());
					}

					function onEachFeature(feature, layer) {
						layer.on({
							mouseover: highlightFeature,
							mouseout: resetHighlight,
							click: zoomToFeature
						});
					}

	    		var mymap$rand = L.map('map-$question-$rand',{
						scrollWheelZoom:false,
						zoomControl: false
					});

	    		L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	    			maxZoom: 18,
	    			attribution: 'Map data &copy; <a href=\"http://openstreetmap.org\">OpenStreetMap</a> contributors, ' +
	    				'<a href=\"http://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>, ' +
	    				'Imagery © <a href=\"http://mapbox.com\">Mapbox</a>',
	    			id: 'mapbox.streets'
	    		}).addTo(mymap$rand);

					L.control.zoom({
					    position: 'topright'
					}).addTo(mymap$rand);

	    		var geojson = L.geoJSON(geojsonFeature,
	    			{
	    			    style: function (feature) {
	    			    	if (feature.properties.category == 'Muito acima da média')
	    			        	return {fillColor: '#1670C1',weight: 1, fillOpacity: .5, color: '#000'};
	    			      else if (feature.properties.category == 'Acima da média')
	    			        	return {fillColor: '#499ce5',weight: 1, fillOpacity: .5, color: '#000'};
									else if (feature.properties.category == 'Abaixo da média')
											return {fillColor: '#aad1f4',weight: 1, fillOpacity: .5, color: '#000'};
									else if (feature.properties.category == 'Muito abaixo da média')
											return {fillColor: '#daecfb',weight: 1, fillOpacity: .5, color: '#000'};
	    			    },
								onEachFeature: onEachFeature
	    			}).addTo(mymap$rand);

	    		mymap$rand.fitBounds(L.geoJSON(geojsonFeature).getBounds());

					var info$rand = L.control({position: 'topleft'});

					info$rand.onAdd = function (mymap$rand) {
						this._div = L.DomUtil.create('div', 'info infobox');
						this.update();
						return this._div;
					};

					// method that we will use to update the control based on feature properties passed
					info$rand.update = function (props) {
						this._div.innerHTML = '<h4>$atributo - $servico</h4>' +  (props ?
							'<span class=infobox_municipio>' + props.Base_MUNIC_2017_NOME_MUNIC + '</span>' +
							'<span class=infobox_categoria>' + props.category + '</span>'";
                    // $atts_aux = [];
                    // foreach ($infobox as $infobox_att) {
                    //     $atts_aux[] = "'<br />$infobox_att: ' + props['$infobox_att']";
                    // }
                    // $js .= implode(" + ", $atts_aux);
                    $js .= "
							: 'Passe o mouse por cima do município');
					};

					";
        if ($data) {
            $js .= "
								info$rand.addTo(mymap$rand);
							";
        }
                    $js .= "
					var legend = L.control({position: 'bottomright'});

					legend.onAdd = function (map) {

						var div = L.DomUtil.create('div', 'info legend'),
							grades = ['Muito acima da média', 'Acima da média', 'Abaixo da média', 'Muito abaixo da média'],
							colors = ['#1670C1','#499ce5','#aad1f4','#daecfb'];

						// loop through our density intervals and generate a label with a colored square for each interval
						for (var i = 0; i < grades.length; i++) {
							div.innerHTML +=
								'<i style=\"background:' + colors[i] + '\"></i> ' +
								grades[i] + '<br>';
						}
						return div;
					};

					";
        if ($data) {
            $js .= "
								legend.addTo(mymap$rand);
							";
        }
        if ($height && $width) {
            $style = "style='height:$height; width:$width'";
        } else {
            $style = "";
        }

          return ( "<div $style class='map' id='map-$question-$rand'></div><script>$js</script>" );

    }

    /**
     * Implements mbi_modal shortcode
     *
     * @param array $atts Shortcode attributes
     *
     * @return string
     */
    function metabase_infosanbas_modal( $atts )
    {

        $id = rand();
        $tipo = $atts['tipo'];

        if($tipo == "1") {
            $texto = "O Índice Igualdade e Não Discriminação foi calculado por um método estatístico denominado Análise de Componentes Principais (procedimento de Análise Variada), pelo qual a informação contida nos indicadores é sintetizada em um conjunto menor de elementos – os componentes principais – a partir do qual o índice é calculado.<br><br>

			No período mais antigo, o primeiro componente principal explicou 48,5% da variância dos dados municipais e, no período mais recente, explicou 47% da variabilidade. Em ambos os períodos, os indicadores com maior peso nesse componente foram os que dizem respeito ao acesso de sub subgrupos populacionais à rede geral de distribuição de água (população rural; população acima de 25 anos de idade sem instrução ou com ensino fundamental incompleto; população com renda domiciliar ‘per capita’ abaixo da linha de pobreza; população de cor ou raça não branca) e o indicador referente à “diferença entre as proporções da população com pelo menos ensino fundamental completo e da população com ensino fundamental incompleto (acima de 25 anos de idade) no acesso à rede geral de distribuição de água.";
        }
        if($tipo == "2") {
            $texto = "O <b>Índice de Igualdade e Não Discriminação</b> é calculado a partir da desagregação de três variáveis do Censo Demográfico: “renda domiciliar per capita”, “nível de escolaridade” e “cor da pele” (ou raça), que deram origem aos indicadores da fórmula de cálculo abaixo.<br><br>

			O cálculo deste <b>Índice de Igualdade e Não Discriminação</b> foi feito a partir de um índice de <em>desigualdade</em>, gerado pelos três critérios acima (renda domiciliar per capita; nível de escolaridade; cor da pele ou raça). A base de cálculo deste índice de desigualdade é a razão entre a proporção de acesso a serviços “pelo menos básicos” de esgotamento sanitário por subgrupos populacionais mais vulneráveis à privação desses serviços, e subgrupos menos vulneráveis à essa privação, conforme mostrado na equação abaixo:<br>
			<img src='https://infosanbas.org.br/wp-content/uploads/2020/07/ql_b810846165f0dd3590ef6a370c58fb4b_l3.png' /><br>
			Onde:<br>
			ID = Índice de desigualdade<br>
			B = Proporção da população branca com acesso a serviços “pelo menos básicos” de esgoto<br>
			N = Proporção da população não branca com acesso a serviços “pelo menos básicos” de esgoto<br>
			SC = Proporção da população com ensino superior completo com acesso a serviços “pelo menos<br> básicos” de esgoto<br>
			SI = Proporção da população sem instrução ou com ensino fundamental incompleto com acesso a serviços “pelo menos básicos” de esgoto<br>
			LR = Proporção da população residente em domicílios com renda domiciliar per capita igual ou superior a 1 salário mínimo com acesso a serviços “pelo menos básicos” de esgoto<br>
			LP = Proporção da população residente em domicílios com renda domiciliar per capita abaixo da linha de pobreza (inferior a 1/4 de salário mínimo) com acesso a serviços “pelo menos básicos” de esgoto<br><br>

			O <b>Índice de Igualdade e Não Discriminação</b> é o índice de desigualdade reescalonado de 0 a 1 e invertido, de forma que quanto maior o seu valor (quanto mais próximo de 1) mais igualitário é o município; quanto menor o valor (mais próximo de 0), maior a desigualdade.";

        }


        $modal = "<button id='legendabtn_$id' data-legenda='$id' class='glossario'>?</button><span id='legendamodal_$id' class='modal'><span class='modal-content'><span class='close'>&times;</span>" . $texto . "</span></span>";

        $modal_script = '
		<script>
		jQuery( document ).ready(
			function ($) {
				$("[id^=legendabtn]").click(function(){

					var indicador = $(this).attr("data-legenda")
					var modal = $("#legendamodal_"+indicador)
					modal.css("display", "block");

					var modal_content = $(".modal-content")
					modal_content.css("display", "block");

					$(".modal-content span").click(function(e){
						modal.css("display", "none");
					});

					window.onclick = function(event) {
					  if (modal.is(event.target)) {
					    modal.css("display", "none");
					  }
					}
				});
			}
		);
		</script>';

        return $modal . $modal_script;

    }

    /**
     * Gets the answer from a Metabase public question
     *
     * @param string $uuid          Shortcode attributes
     * @param array  $parameters    Parameters to be passed to metabase. Format: array()
     * @param string $export_format 'json' or 'csv'
     *
     * @return array
     */
    private function metabase_get_answer( $uuid, $parameters = null, $export_format = 'json')
    {
        $export_format = 'json';

        if ($parameters) {
            $param = null;
            foreach ($parameters as $parameter_key => $parameter_value) {
                $param[] = array(
                 "type" => "category",
                 "target" => array(
                     "variable",
                     array("template-tag", $parameter_key)
                 ),
                "value" => $parameter_value
                  );
            }
            $parameters = "?parameters=" . urlencode(json_encode($param));
        }

        // print_r($parameters);

        $ch = curl_init();
        // echo get_option( 'mbi_metabase_url' ) . "/api/public/card/" . $uuid . "/query/" . $export_format . "?parameters=" . urlencode("[".json_encode($param) . "]");
        curl_setopt($ch, CURLOPT_URL, get_option('mbi_metabase_url') . "/api/public/card/" . $uuid . "/query/" . $export_format . $parameters);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Accept: application/json'));
        // curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $res = curl_exec($ch);
        curl_close($ch);
        // echo $res;
        $res = json_decode($res);
        return ($res);
    }

    /**
     * Create posts for municicípios
     *
     * @return bool
     */
    public function create_municipios()
    {

        global $MUNICIPIOS, $ATRIBUTOS, $MUNICIPIOS_PARENT_ID, $CATEGORY_ID;

        foreach ($MUNICIPIOS as $municipio) {

            $post_content = '
				'. $municipio .' está localizado na Região Metropolitana de Belo Horizonte e em 2017 tinha [mbinumber question="fef3036e-df1a-4ece-8925-b8da8511fd0d" municipio="'. $municipio .' (MG)" ano=2017 attname="População"] habitantes.

				[mbimapsingle map="ca330686-780d-449e-bfce-0de3ee1a3387" municipio="'. $municipio .'"]
				<h3 class="servico">Água</h3>
				Desempenho municipal nos índices relativos aos atributos normativos do Direito Humano à Água:

				[mbibar question="aeb30c92-8d15-409d-bd29-0f300975818b" servico="Água" municipio="'. $municipio .'"]

				<div class=alertbox>A seguir, entenda a composição dos atributos normativos e o desempenho de '. $municipio .' em cada um dos indicadores:</div>

				[mbimunicipio question="4037ea92-d095-4c30-989b-08c129aebe3b" servico="Água" municipio="'. $municipio .'" ranking="0d5903b2-f1d1-4c39-9e7d-190c554984ed"]
				<h3 class="servico">Esgoto</h3>
				Desempenho municipal nos índices relativos aos atributos normativos do Direito Humano ao Esgotamento Sanitário:

				[mbibar question="aeb30c92-8d15-409d-bd29-0f300975818b" servico="Esgoto" municipio="'. $municipio .'"]

				<div class=alertbox>A seguir, entenda a composição dos atributos normativos e o desempenho de '. $municipio .' em cada um dos indicadores:</div>

				[mbimunicipio question="59590568-3eea-4758-b806-43032d767210" servico="Esgoto" municipio="'. $municipio .'" ranking="0d5903b2-f1d1-4c39-9e7d-190c554984ed" ]
			';

            $postarr = array(
            'post_title' => $municipio,
            'post_status' => "publish",
            'post_type' => "manual_documentation",
            'post_content' => $post_content,
            'post_parent' => $MUNICIPIOS_PARENT_ID,
            'tax_input' => array('manualdocumentationcategory' => $CATEGORY_ID),
            );
            wp_insert_post($postarr);
        }

        return true;
    }

    /**
     * Create posts for municicípios
     *
     * @return bool
     */
    public function create_atributos()
    {

        global $ATRIBUTOS, $SERVICO_PARENT_ID, $CATEGORY_ID;
        $servicos = array('Água', 'Esgoto');

        foreach ($servicos as $servico) {
            foreach ($ATRIBUTOS[$servico] as $atributo => $values) {

                $post_content = '
					[mbiatributo servico="'.$servico.'" atributo="'.$atributo.'"]

					Os mapas abaixo mostram os municípios da Região Metropolitana de Belo Horizonte (RMBH) divididos em quatro categorias em relação à média do valor do atributo para a RMBH:
					<ul>
						<li><b>Muito abaixo da média da RMBH</b></li>
						<li><b>Abaixo da média da RMBH</b></li>
						<li><b>Acima da média da RMBH</b></li>
						<li><b>Muito acima da média da RMBH</b></li>
					</ul>

					<h4 class="atributo">Período Mais Antigo</h4>
					[mbimap question="81519291-fc0b-4441-85e8-50383a2b7e09" question_key="CodMun" question_value="Classificação (antigo)" map="6c300307-0b33-4321-a8cc-a948fc04c1a0" map_key="CodMun" atributo="'.$atributo.'" servico="'.$servico.'" infobox="Índice (antigo)#Média (antigo)"]
					<p class=footnote>* Muito acima e muito abaixo da média se referem à valores maiores do que a média mais um desvio padrão, e menores que a média menos um desvio padrão, respectivamente.</p>

					<h4 class="atributo">Período Mais Recente</h4>
					[mbimap question="81519291-fc0b-4441-85e8-50383a2b7e09" question_key="CodMun" question_value="Classificação (recente)" map="6c300307-0b33-4321-a8cc-a948fc04c1a0" map_key="CodMun" atributo="'.$atributo.'" servico="'.$servico.'" infobox="Índice (recente)#Média (recente)"]
					<p class=footnote>* Muito acima e muito abaixo da média se referem à valores maiores do que a média mais um desvio padrão, e menores que a média menos um desvio padrão, respectivamente.</p>

					<h4 class="atributo">Ranking dos municípios</h4>
					[mbitable question="0d5903b2-f1d1-4c39-9e7d-190c554984ed" atributo="'.$atributo.'" servico="'.$servico.'"]
				';

                $postarr = array(
                 'post_title' => $atributo,
                 'post_status' => "publish",
                 'post_type' => "manual_documentation",
                 'post_content' => $post_content,
                 'post_parent' => $SERVICO_PARENT_ID[$servico],
                 'tax_input' => array('manualdocumentationcategory' => $CATEGORY_ID),
                );
                wp_insert_post($postarr);
            }
        }
        return true;
    }

    /**
     * Delete posts for municicípios and atributos.
     *
     * @return bool
     */
    public function delete_municipios()
    {

        global $MUNICIPIOS, $ATRIBUTOS, $MUNICIPIOS_PARENT_ID;

        $args = array(
        'post_parent' => $MUNICIPIOS_PARENT_ID,
        'post_type' => 'manual_documentation',
        'posts_per_page' => -1
        );

        $query = new WP_Query($args);

        if ($query->have_posts() ) {
            foreach ($query->posts as $post) {
                wp_delete_post($post->ID);
            }
        }

        return true;
    }

    /**
     * Delete posts for atributos.
     *
     * @return bool
     */
    public function delete_atributos()
    {

        global $SERVICO_PARENT_ID;
        $servicos = array('Água','Esgoto');

        foreach ($servicos as $servico) {

            $args = array(
            'post_parent' => $SERVICO_PARENT_ID[$servico],
            'post_type' => 'manual_documentation',
            'posts_per_page' => -1
            );

            $query = new WP_Query($args);

            if ($query->have_posts() ) {
                foreach ($query->posts as $post) {
                    wp_delete_post($post->ID);
                }
            }
        }
        return true;
    }

    /**
     * Load frontend CSS.
     *
     * @access public
     * @return void
     * @since  1.0.0
     */
    public function enqueue_styles()
    {
        wp_register_style($this->_token . '-frontend', esc_url($this->assets_url) . 'css/frontend.css', array(), $this->_version);
        wp_enqueue_style($this->_token . '-frontend');

        // leaflet - already registered by the theme
        // wp_register_style( $this->_token . '-leaflet', esc_url( $this->assets_url ) . 'leaflet/leaflet.css', array(), $this->_version );
        // wp_enqueue_style( $this->_token . '-leaflet' );

        // datatables
        wp_register_style($this->_token . '-datatables', '//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css', array(), $this->_version);
        wp_enqueue_style($this->_token . '-datatables');

    } // End enqueue_styles ()

    /**
     * Load frontend Javascript.
     *
     * @access public
     * @return void
     * @since  1.0.0
     */
    public function enqueue_scripts()
    {
        // wp_register_script( $this->_token . '-frontend', esc_url( $this->assets_url ) . 'js/frontend.js', array( 'jquery' ), $this->_version, true );
        // wp_enqueue_script( $this->_token . '-frontend' );

        // leaflet
        wp_register_script($this->_token . '-frontend', esc_url($this->assets_url) . 'js/frontend.js', array( 'jquery' ), $this->_version, true);
        wp_enqueue_script($this->_token . '-frontend');

        // leaflet already registered by the theme
        // wp_register_script( $this->_token . '-leaflet', esc_url( $this->assets_url ) . 'leaflet/leaflet.js', array( 'jquery' ), $this->_version, false );
        // wp_enqueue_script( $this->_token . '-leaflet' );

        // datatables
        wp_register_script($this->_token . '-datatables', '//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js', array( 'jquery' ), $this->_version, false);
        wp_enqueue_script($this->_token . '-datatables');

    } // End enqueue_scripts ()

    /**
     * Admin enqueue style.
     *
     * @param string $hook Hook parameter.
     *
     * @return void
     */
    public function admin_enqueue_styles( $hook = '' )
    {
        wp_register_style($this->_token . '-admin', esc_url($this->assets_url) . 'css/admin.css', array(), $this->_version);
        wp_enqueue_style($this->_token . '-admin');
    } // End admin_enqueue_styles ()

    /**
     * Load admin Javascript.
     *
     * @access public
     *
     * @param string $hook Hook parameter.
     *
     * @return void
     * @since  1.0.0
     */
    public function admin_enqueue_scripts( $hook = '' )
    {
        wp_register_script($this->_token . '-admin', esc_url($this->assets_url) . 'js/admin' . $this->script_suffix . '.js', array( 'jquery' ), $this->_version, true);
        wp_enqueue_script($this->_token . '-admin');
    } // End admin_enqueue_scripts ()

    /**
     * Load plugin localisation
     *
     * @access public
     * @return void
     * @since  1.0.0
     */
    public function load_localisation()
    {
        load_plugin_textdomain('metabase-infosanbas', false, dirname(plugin_basename($this->file)) . '/lang/');
    } // End load_localisation ()

    /**
     * Load plugin textdomain
     *
     * @access public
     * @return void
     * @since  1.0.0
     */
    public function load_plugin_textdomain()
    {
        $domain = 'metabase-infosanbas';

        $locale = apply_filters('plugin_locale', get_locale(), $domain);

        load_textdomain($domain, WP_LANG_DIR . '/' . $domain . '/' . $domain . '-' . $locale . '.mo');
        load_plugin_textdomain($domain, false, dirname(plugin_basename($this->file)) . '/lang/');
    } // End load_plugin_textdomain ()

    /**
     * Main Metabase_Infosanbas Instance
     *
     * Ensures only one instance of Metabase_Infosanbas is loaded or can be loaded.
     *
     * @param string $file    File instance.
     * @param string $version Version parameter.
     *
     * @return Object Metabase_Infosanbas instance
     * @see    Metabase_Infosanbas()
     * @since  1.0.0
     * @static
     */
    public static function instance( $file = '', $version = '1.0.0' )
    {
        if (is_null(self::$_instance) ) {
            self::$_instance = new self($file, $version);
        }

        return self::$_instance;
    } // End instance ()

    /**
     * Cloning is forbidden.
     *
     * @since 1.0.0
     */
    public function __clone()
    {
        _doing_it_wrong(__FUNCTION__, esc_html(__('Cloning of Metabase_Infosanbas is forbidden')), esc_attr($this->_version));

    } // End __clone ()

    /**
     * Unserializing instances of this class is forbidden.
     *
     * @since 1.0.0
     */
    public function __wakeup()
    {
        _doing_it_wrong(__FUNCTION__, esc_html(__('Unserializing instances of Metabase_Infosanbas is forbidden')), esc_attr($this->_version));
    } // End __wakeup ()

    /**
     * Installation. Runs on activation.
     *
     * @access public
     * @return void
     * @since  1.0.0
     */
    public function install()
    {
        $this->_log_version_number();
    } // End install ()

    /**
     * Log the plugin version number.
     *
     * @access public
     * @return void
     * @since  1.0.0
     */
	private function _log_version_number() { //phpcs:ignore
        update_option($this->_token . '_version', $this->_version);
} // End _log_version_number ()

}
