<?php

global $INDICADORES;
$INDICADORES = array(
  'Esgoto' => array(
    1 => array('Existência de instrumento regulador do serviço de esgotamento sanitário no município', 'SEIS 2011 e 2014', 'BOOLEANO', '', ''),
    2 => array('Existência de Conselho Municipal de Saneamento ou outro órgão que permita a participação da população nas decisões sobre o saneamento básico', 'SEIS 2009, 2011 e 2014', 'BOOLEANO', '', ''),
    3 => array('Existência de algum tipo de tarifa social ou subsídio no serviço de esgotamento sanitário no distrito', 'SEIS 2009, 2011 e 2014', 'BOOLEANO', '', ''),
    4 => array('Proporção de atendimento à reclamações e solicitações de serviços de abastecimento de água e esgotamento sanitário em 2010 e 2014', 'SNIS 2010 e 2014', 'MAIOR', '', '%'),
    5 => array('Diferença (%) no atendimento à reclamações e solicitações de serviços de abastecimento de água e esgotamento sanitário entre 2010 e 2014', 'SNIS 2010 e 2014', 'MAIOR', '', ''),
    6 => array('Proporção da população total com acesso a serviços "pelo menos básicos" de esgoto (segundo a classificação JMP) em 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', 'O acesso a serviços "pelo menos básicos" de esgotamento sanitário é definido como o uso de instalações melhoradas que não são compartilhadas com outras famílias. Instalações melhoradas incluem: vasos ou descargas sanitárias conectadas a redes coletoras de esgoto, a fossas sépticas, fossas rudimentares, fossas secas ventiladas e fossas secas com laje ou plataforma e privadas de compostagem (incluindo sistemas de fossa dupla e sistemas baseados em reservatórios). Referência: WHO/UNICEF/JMP. Progress on Drinking Water, Sanitation and Hygiene. Update and SDG Baselines. 2017. Acesso em: https://www.who.int/mediacentre/news/releases/2017/launch-version-report-jmp-water-sanitation-hygiene.pdf.', '%'),
    7 => array('Proporção da população urbana com acesso a serviços "pelo menos básicos" de esgoto (segundo a classificação JMP) em 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', 'O acesso a serviços "pelo menos básicos" de esgotamento sanitário é definido como o uso de instalações melhoradas que não são compartilhadas com outras famílias. Instalações melhoradas incluem: vasos ou descargas sanitárias conectadas a redes coletoras de esgoto, a fossas sépticas, fossas rudimentares, fossas secas ventiladas e fossas secas com laje ou plataforma e privadas de compostagem (incluindo sistemas de fossa dupla e sistemas baseados em reservatórios). Referência: WHO/UNICEF/JMP. Progress on Drinking Water, Sanitation and Hygiene. Update and SDG Baselines. 2017. Acesso em: https://www.who.int/mediacentre/news/releases/2017/launch-version-report-jmp-water-sanitation-hygiene.pdf.', '%'),
    8 => array('Proporção da população rural com acesso a serviços "pelo menos básicos" de esgoto (segundo a classificação JMP) em 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', 'O acesso a serviços "pelo menos básicos" de esgotamento sanitário é definido como o uso de instalações melhoradas que não são compartilhadas com outras famílias. Instalações melhoradas incluem: vasos ou descargas sanitárias conectadas a redes coletoras de esgoto, a fossas sépticas, fossas rudimentares, fossas secas ventiladas e fossas secas com laje ou plataforma e privadas de compostagem (incluindo sistemas de fossa dupla e sistemas baseados em reservatórios). Referência: WHO/UNICEF/JMP. Progress on Drinking Water, Sanitation and Hygiene. Update and SDG Baselines. 2017. Acesso em: https://www.who.int/mediacentre/news/releases/2017/launch-version-report-jmp-water-sanitation-hygiene.pdf.', '%'),
    9 => array('Proporção da população acima de 25 anos de idade sem instrução ou com ensino fundamental incompleto (0 a 7 anos de estudo) com acesso a serviços "pelo menos básicos" de esgoto (segundo a classificação JMP) em 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', '%'),
    10 => array('Proporção da população com renda domiciliar per capita abaixo da linha de  pobreza (inferior a 1/4 SM) com acesso a serviços "pelo menos básicos" de esgoto (segundo a classificação JMP) em 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', 'O acesso a serviços "pelo menos básicos" de esgotamento sanitário é definido como o uso de instalações melhoradas que não são compartilhadas com outras famílias. Instalações melhoradas incluem: vasos ou descargas sanitárias conectadas a redes coletoras de esgoto, a fossas sépticas, fossas rudimentares, fossas secas ventiladas e fossas secas com laje ou plataforma e privadas de compostagem (incluindo sistemas de fossa dupla e sistemas baseados em reservatórios). Referência: WHO/UNICEF/JMP. Progress on Drinking Water, Sanitation and Hygiene. Update and SDG Baselines. 2017. Acesso em: https://www.who.int/mediacentre/news/releases/2017/launch-version-report-jmp-water-sanitation-hygiene.pdf.', '%'),
    11 => array('Proporção da população de cor ou raça não branca com acesso a serviços "pelo menos básicos" de esgoto (segundo a classificação JMP) em 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', 'O acesso a serviços "pelo menos básicos" de esgotamento sanitário é definido como o uso de instalações melhoradas que não são compartilhadas com outras famílias. Instalações melhoradas incluem: vasos ou descargas sanitárias conectadas a redes coletoras de esgoto, a fossas sépticas, fossas rudimentares, fossas secas ventiladas e fossas secas com laje ou plataforma e privadas de compostagem (incluindo sistemas de fossa dupla e sistemas baseados em reservatórios). Referência: WHO/UNICEF/JMP. Progress on Drinking Water, Sanitation and Hygiene. Update and SDG Baselines. 2017. Acesso em: https://www.who.int/mediacentre/news/releases/2017/launch-version-report-jmp-water-sanitation-hygiene.pdf.', '%'),
    12 => array('Diferença (%) no acesso a serviços "pelo menos básicos" de esgoto (segundo a classificação JMP), por parte da população total, entre 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', 'O acesso a serviços "pelo menos básicos" de esgotamento sanitário é definido como o uso de instalações melhoradas que não são compartilhadas com outras famílias. Instalações melhoradas incluem: vasos ou descargas sanitárias conectadas a redes coletoras de esgoto, a fossas sépticas, fossas rudimentares, fossas secas ventiladas e fossas secas com laje ou plataforma e privadas de compostagem (incluindo sistemas de fossa dupla e sistemas baseados em reservatórios). Referência: WHO/UNICEF/JMP. Progress on Drinking Water, Sanitation and Hygiene. Update and SDG Baselines. 2017. Acesso em: https://www.who.int/mediacentre/news/releases/2017/launch-version-report-jmp-water-sanitation-hygiene.pdf.', ''),
    13 => array('Diferença (%) da população urbana  no acesso a serviços "pelo menos básicos" de esgoto (segundo a classificação JMP) entre 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', 'O acesso a serviços "pelo menos básicos" de esgotamento sanitário é definido como o uso de instalações melhoradas que não são compartilhadas com outras famílias. Instalações melhoradas incluem: vasos ou descargas sanitárias conectadas a redes coletoras de esgoto, a fossas sépticas, fossas rudimentares, fossas secas ventiladas e fossas secas com laje ou plataforma e privadas de compostagem (incluindo sistemas de fossa dupla e sistemas baseados em reservatórios). Referência: WHO/UNICEF/JMP. Progress on Drinking Water, Sanitation and Hygiene. Update and SDG Baselines. 2017. Acesso em: https://www.who.int/mediacentre/news/releases/2017/launch-version-report-jmp-water-sanitation-hygiene.pdf.', ''),
    14 => array('Diferença (%) da população  rural  no acesso a serviços "pelo menos básicos" de esgoto (segundo a classificação JMP) entre 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', 'O acesso a serviços "pelo menos básicos" de esgotamento sanitário é definido como o uso de instalações melhoradas que não são compartilhadas com outras famílias. Instalações melhoradas incluem: vasos ou descargas sanitárias conectadas a redes coletoras de esgoto, a fossas sépticas, fossas rudimentares, fossas secas ventiladas e fossas secas com laje ou plataforma e privadas de compostagem (incluindo sistemas de fossa dupla e sistemas baseados em reservatórios). Referência: WHO/UNICEF/JMP. Progress on Drinking Water, Sanitation and Hygiene. Update and SDG Baselines. 2017. Acesso em: https://www.who.int/mediacentre/news/releases/2017/launch-version-report-jmp-water-sanitation-hygiene.pdf.', ''),
    15 => array('Diferença (%) entre as proporções de população urbana e rural  no acesso a serviços "pelo menos básicos" de esgoto (segundo a classificação JMP) em 2000 e 2010', 'Censo 2000 e 2010', 'MENOR', 'O acesso a serviços "pelo menos básicos" de esgotamento sanitário é definido como o uso de instalações melhoradas que não são compartilhadas com outras famílias. Instalações melhoradas incluem: vasos ou descargas sanitárias conectadas a redes coletoras de esgoto, a fossas sépticas, fossas rudimentares, fossas secas ventiladas e fossas secas com laje ou plataforma e privadas de compostagem (incluindo sistemas de fossa dupla e sistemas baseados em reservatórios). Referência: WHO/UNICEF/JMP. Progress on Drinking Water, Sanitation and Hygiene. Update and SDG Baselines. 2017. Acesso em: https://www.who.int/mediacentre/news/releases/2017/launch-version-report-jmp-water-sanitation-hygiene.pdf.', ''),
    16 => array('Diferença (%) da população sem instrução ou com ensino fundamental incompleto (acima de 25 anos de idade) no acesso a serviços "pelo menos básicos" de esgoto (segundo a classificação JMP) entre 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '. Instalações sanitárias melhoradas segundo a classificação JMP.', ''),
    17 => array('Diferença (%) entre as proporções da população com pelo menos ensino fundamental completo e da população com ensino fundamental incompleto (acima de 25 anos de idade)  no acesso a serviços "pelo menos básicos" de esgoto (segundo a classificação JMP) em 2000 e 2010', 'Censo 2000 e 2010', 'MENOR', 'Maior escolaridade se refere à população acima de 25 anos de idade com pelo menos ensino fundamental completo. Menor escolaridade se refere à população acima de 25 anos de idade sem instrução ou com ensino fundamental incompleto (0 a 7 anos de estudo). Instalações sanitárias melhoradas segundo a classificação JMP.', ''),
    18 => array('Diferença (%) da população abaixo da linha de pobreza  no acesso a serviços "pelo menos básicos" de esgoto (segundo a classificação JMP) entre 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', 'O acesso a serviços "pelo menos básicos" de esgotamento sanitário é definido como o uso de instalações melhoradas que não são compartilhadas com outras famílias. Instalações melhoradas incluem: vasos ou descargas sanitárias conectadas a redes coletoras de esgoto, a fossas sépticas, fossas rudimentares, fossas secas ventiladas e fossas secas com laje ou plataforma e privadas de compostagem (incluindo sistemas de fossa dupla e sistemas baseados em reservatórios). Referência: WHO/UNICEF/JMP. Progress on Drinking Water, Sanitation and Hygiene. Update and SDG Baselines. 2017. Acesso em: https://www.who.int/mediacentre/news/releases/2017/launch-version-report-jmp-water-sanitation-hygiene.pdf.', ''),
    19 => array('Diferença (%) entre as proporções da população acima da linha de pobreza e da população abaixo da linha de pobreza  no acesso a serviços "pelo menos básicos" de esgoto (segundo a classificação JMP) em 2000 e 2010', 'Censo 2000 e 2010', 'MENOR', 'O acesso a serviços "pelo menos básicos" de esgotamento sanitário é definido como o uso de instalações melhoradas que não são compartilhadas com outras famílias. Instalações melhoradas incluem: vasos ou descargas sanitárias conectadas a redes coletoras de esgoto, a fossas sépticas, fossas rudimentares, fossas secas ventiladas e fossas secas com laje ou plataforma e privadas de compostagem (incluindo sistemas de fossa dupla e sistemas baseados em reservatórios). Referência: WHO/UNICEF/JMP. Progress on Drinking Water, Sanitation and Hygiene. Update and SDG Baselines. 2017. Acesso em: https://www.who.int/mediacentre/news/releases/2017/launch-version-report-jmp-water-sanitation-hygiene.pdf.', ''),
    20 => array('Diferença (%) da população não branca  no acesso a serviços "pelo menos básicos" de esgoto (segundo a classificação JMP) entre 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', 'O acesso a serviços "pelo menos básicos" de esgotamento sanitário é definido como o uso de instalações melhoradas que não são compartilhadas com outras famílias. Instalações melhoradas incluem: vasos ou descargas sanitárias conectadas a redes coletoras de esgoto, a fossas sépticas, fossas rudimentares, fossas secas ventiladas e fossas secas com laje ou plataforma e privadas de compostagem (incluindo sistemas de fossa dupla e sistemas baseados em reservatórios). Referência: WHO/UNICEF/JMP. Progress on Drinking Water, Sanitation and Hygiene. Update and SDG Baselines. 2017. Acesso em: https://www.who.int/mediacentre/news/releases/2017/launch-version-report-jmp-water-sanitation-hygiene.pdf.', ''),
    21 => array('Diferença (%) entre as proporções de população branca e não branca  no acesso a serviços "pelo menos básicos" de esgoto (segundo a classificação JMP) em 2000 e 2010', 'Censo 2000 e 2010', 'MENOR', 'O acesso a serviços "pelo menos básicos" de esgotamento sanitário é definido como o uso de instalações melhoradas que não são compartilhadas com outras famílias. Instalações melhoradas incluem: vasos ou descargas sanitárias conectadas a redes coletoras de esgoto, a fossas sépticas, fossas rudimentares, fossas secas ventiladas e fossas secas com laje ou plataforma e privadas de compostagem (incluindo sistemas de fossa dupla e sistemas baseados em reservatórios). Referência: WHO/UNICEF/JMP. Progress on Drinking Water, Sanitation and Hygiene. Update and SDG Baselines. 2017. Acesso em: https://www.who.int/mediacentre/news/releases/2017/launch-version-report-jmp-water-sanitation-hygiene.pdf.', ''),
    22 => array('Proporção da população total com acesso a banheiro de uso exclusivo no domicílio em 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', '%'),
    23 => array('Proporção da população urbana com acesso a banheiro de uso exclusivo no domicílio em 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', '%'),
    24 => array('Proporção da população rural com acesso a banheiro de uso exclusivo no domicílio em 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', '%'),
    25 => array('Proporção da população acima de 25 anos de idade sem instrução ou com ensino fundamental incompleto (0 a 7 anos de estudo) com acesso a banheiro de uso exclusivo no domicílio em 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', '%'),
    26 => array('Proporção da população com renda domiciliar per capita abaixo da linha de pobreza (inferior a 1/4 SM) com acesso a banheiro de uso exclusivo no domicílio em 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', '%'),
    27 => array('Proporção da população de cor ou raça não branca com acesso a banheiro de uso exclusivo no domicílio em 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', '%'),
    28 => array('Diferença (%) no acesso a banheiro de uso exclusivo no domicílio, por parte da população total, entre 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', ''),
    29 => array('Diferença (%) da população urbana no acesso a banheiro de uso exclusivo no domicílio entre 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', ''),
    30 => array('Diferença (%) da população  rural  no acesso a banheiro de uso exclusivo no domicílio entre 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', ''),
    31 => array('Diferença (%) entre as proporções de população urbana e rural  no acesso a banheiro de uso exclusivo no domicílio em 2000 e 2010', 'Censo 2000 e 2010', 'MENOR', '', ''),
    32 => array('Diferença (%) da população sem instrução ou com ensino fundamental incompleto (acima de 25 anos de idade) no acesso a banheiro de uso exclusivo no domicílio entre 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', ''),
    33 => array('Diferença (%) entre as proporções de população com pelo menos ensino fundamental completo e a população com ensino fundamental incompleto (acima de 25 anos de idade)  no acesso a banheiro de uso exclusivo no domicílio em 2000 e 2010', 'Censo 2000 e 2010', 'MENOR', 'Maior escolaridade se refere à população acima de 25 anos de idade com pelo menos ensino fundamental completo. Menor escolaridade se refere à população acima de 25 anos de idade sem instrução ou com ensino fundamental incompleto (0 a 7 anos de estudo).', ''),
    34 => array('Diferença (%) da população abaixo da linha de pobreza no acesso a banheiro de uso exclusivo no domicílio entre 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', ''),
    35 => array('Diferença (%) entre as proporções da população acima da linha de pobreza e da população abaixo da linha de pobreza  no acesso a banheiro de uso exclusivo no domicílio em 2000 e 2010', 'Censo 2000 e 2010', 'MENOR', '', ''),
    36 => array('Diferença (%) da população não branca no acesso a banheiro de uso exclusivo no domicílio entre 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', ''),
    37 => array('Diferença (%) entre as proporções de população branca e a população não branca  no acesso a banheiro de uso exclusivo no domicílio em 2000 e 2010', 'Censo 2000 e 2010', 'MENOR', '', ''),
    38 => array('Média anual de incidências (‰) de internações do SUS por Doenças Relacionadas ao Saneamento Inadequado (DRSAI) - 2000 a 2007 e 2008 a 2015 (ASR6 e ESR3)', 'DATASUS 2000 a 2016', 'MENOR', 'DRSAI: Cólera, Febres tifóide e paratifóide, Shiguelose, Amebíase, Diarréia e gastroenterite origem infecc presumível, Outras doenças infecciosas intestinais, Leptospirose icterohemorrágica, Outras formas de leptospirose, Leptospirose não especificada, Tracoma, Febre amarela, Dengue [dengue clásssico], Febre hemorrágica devida ao vírus da dengue, Restante de outr febr arbovírus febr hemor vírus, Outras hepatites virais, Micoses, Malária por Plasmodium falciparum, Malária por Plasmodium vivax, Malária por Plasmodium malariae, Outras formas malária conf exames parasitológ, Malária não especificada, Leishmaniose visceral, Leishmaniose cutânea, Leishmaniose cutâneo-mucosa, Leishmaniose não especificada, Tripanossomíase, Esquistossomose, Outras infestações por trematódeos, Equinococose, Dracunculíase, Filariose, Ancilostomíase, Outras helmintíases, Outras doenças infecciosas e parasitárias, Conjuntivite e outros transtornos da conjuntiva, Infecções da pele e do tecido subcutâneo, Outras doenças da pele e do tecido subcutâneo', '‰'),
    39 => array('Diferença da média anual de incidências (‰) de internações do SUS por Doenças Relacionadas ao Saneamento Inadequado (DRSAI) entre os períodos 2000-2007 e 2008-2015', 'DATASUS 2000 a 2016', 'MAIOR', 'DRSAI: Cólera, Febres tifóide e paratifóide, Shiguelose, Amebíase, Diarréia e gastroenterite origem infecc presumível, Outras doenças infecciosas intestinais, Leptospirose icterohemorrágica, Outras formas de leptospirose, Leptospirose não especificada, Tracoma, Febre amarela, Dengue [dengue clásssico], Febre hemorrágica devida ao vírus da dengue, Restante de outr febr arbovírus febr hemor vírus, Outras hepatites virais, Micoses, Malária por Plasmodium falciparum, Malária por Plasmodium vivax, Malária por Plasmodium malariae, Outras formas malária conf exames parasitológ, Malária não especificada, Leishmaniose visceral, Leishmaniose cutânea, Leishmaniose cutâneo-mucosa, Leishmaniose não especificada, Tripanossomíase, Esquistossomose, Outras infestações por trematódeos, Equinococose, Dracunculíase, Filariose, Ancilostomíase, Outras helmintíases, Outras doenças infecciosas e parasitárias, Conjuntivite e outros transtornos da conjuntiva, Infecções da pele e do tecido subcutâneo, Outras doenças da pele e do tecido subcutâneo', '‰'),
  ),
  'Água' => array(
    1 => array('Existência de instrumento regulador do serviço de abastecimento de água no município', 'ARSAE-MG, CBH-Rio das Velhas e SEIS 2011 e 2014', 'BOOLEANO', '', ''),
    3 => array('Existência de Conselho Municipal de Saneamento ou outro órgão que permita a participação da população nas decisões sobre o saneamento básico', 'SEIS 2009, 2011 e 2014', 'BOOLEANO', '', ''),
    4 => array('Percentual de distritos com tarifa social ou outro subsídio no serviço de abastecimento de água', 'SEIS 2009, 2011 e 2014', 'MAIOR', '', '%'),
    5 => array('Proporção de atendimento à reclamações e solicitações de serviços de abastecimento de água e esgotamento sanitário em 2010 e 2014', 'SNIS 2010 e 2014', 'MAIOR', '', '%'),
    6 => array('Diferença (%) na proporção de serviços executados em relação à quantidade de reclamações ou solicitações entre 2010 e 2014', 'SNIS 2010 e 2014', 'MAIOR', '', ''),
    7 => array('Proporção da população total com acesso à rede geral de distribuição de água em 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', '%'),
    8 => array('Proporção da população urbana com acesso  à rede geral de distribuição de água em 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', '%'),
    9 => array('Proporção da população rural com acesso  à rede geral de distribuição de água em 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', '%'),
    10 => array('Proporção da população acima de 25 anos de idade sem instrução ou com ensino fundamental incompleto (0 a 7 anos de estudo) com acesso  à rede geral de distribuição de água em 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', '%'),
    11 => array('Proporção da população com renda domiciliar per capita abaixo da linha de pobreza (inferior a 1/4 SM) com acesso  à rede geral de distribuição de água em 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', '%'),
    12 => array('Proporção da população de cor ou raça não branca com acesso à rede geral de distribuição de água em 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', '%'),
    13 => array('Diferença (%) no acesso à rede geral de distribuição de água, por parte da população total, em 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', ''),
    14 => array('Diferença (%) da população urbana no acesso à rede geral de distribuição de água entre 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', ''),
    15 => array('Diferença (%) da população  rural no acesso à rede geral de distribuição de água entre 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', ''),
    16 => array('Diferença (%) entre as proporções de população urbana e rural no acesso à rede geral de distribuição de água em 2000 e 2010', 'Censo 2000 e 2010', 'MENOR', '', ''),
    17 => array('Diferença (%) da população sem instrução ou com ensino fundamental incompleto (acima de 25 anos de idade) no acesso à rede geral de distribuição de água entre 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', ''),
    18 => array('Diferença (%) entre as proporções da população com pelo menos ensino fundamental completo e da população com ensino fundamental incompleto (acima de 25 anos de idade) no acesso à rede geral de distribuição de água em 2000 e 2010', 'Censo 2000 e 2010', 'MENOR', '', ''),
    19 => array('Diferença (%) da população abaixo da linha de pobreza no acesso à rede geral de distribuição de água entre 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', ''),
    20 => array('Diferença (%)  entre as proporções da população acima da linha de pobreza e da população abaixo da linha de pobreza no acesso à rede geral de distribuição de água em 2000 e 2010', 'Censo 2000 e 2010', 'MENOR', '', ''),
    21 => array('Diferença (%) da população não branca no acesso à rede geral de distribuição de água entre 2000 e 2010', 'Censo 2000 e 2010', 'MAIOR', '', ''),
    22 => array('Diferença (%) entre as proporções de população branca e não branca no acesso à rede geral de distribuição de água em 2000 e 2010', 'Censo 2000 e 2010', 'MENOR', '', ''),
    24 => array('Diferença (%) no número de amostras analisadas (cloro residual, turbidez e coliformes totais) entre 2010 e 2014', 'SNIS 2010 e 2014', 'MAIOR', '', ''),
    25 => array('Proporção de amostras dentro dos padrões de potabilidade coletadas durante o ano no município em 2007 e 2013 (turbidez, cloro residual livre, fluoreto, coliformes, coliformes totais, escherichia coli, bactérias heterotróficas)', 'SISAGUA 2007 e 2013', 'MAIOR', '', '%'),
    26 => array('Diferença entre 2007 e 2013 na proporção de amostras dentro dos padrões de potabilidade coletadas durante o ano no município  (turbidez, cloro residual livre, fluoreto, coliformes, coliformes totais, escherichia coli, bactérias heterotróficas)', 'SISAGUA 2007 e 2013', 'MAIOR', '', ''),
    27 => array('Média anual de incidências (‰) de internações do SUS por Doenças Relacionadas ao Saneamento Inadequado (DRSAI) - 2000-2007 e 2008-2015', 'DATASUS 2000 a 2016', 'MENOR', 'DRSAI: Cólera, Febres tifóide e paratifóide, Shiguelose, Amebíase, Diarréia e gastroenterite origem infecc presumível, Outras doenças infecciosas intestinais, Leptospirose icterohemorrágica, Outras formas de leptospirose, Leptospirose não especificada, Tracoma, Febre amarela, Dengue [dengue clásssico], Febre hemorrágica devida ao vírus da dengue, Restante de outr febr arbovírus febr hemor vírus, Outras hepatites virais, Micoses, Malária por Plasmodium falciparum, Malária por Plasmodium vivax, Malária por Plasmodium malariae, Outras formas malária conf exames parasitológ, Malária não especificada, Leishmaniose visceral, Leishmaniose cutânea, Leishmaniose cutâneo-mucosa, Leishmaniose não especificada, Tripanossomíase, Esquistossomose, Outras infestações por trematódeos, Equinococose, Dracunculíase, Filariose, Ancilostomíase, Outras helmintíases, Outras doenças infecciosas e parasitárias, Conjuntivite e outros transtornos da conjuntiva, Infecções da pele e do tecido subcutâneo, Outras doenças da pele e do tecido subcutâneo', '‰'),
    28 => array('Diferença da média anual de incidências (‰) de internações do SUS por Doenças Relacionadas ao Saneamento Inadequado (DRSAI) entre os períodos 2000-2007 e 2008-2015', 'DATASUS 2000 a 2016', 'MAIOR', 'DRSAI: Cólera, Febres tifóide e paratifóide, Shiguelose, Amebíase, Diarréia e gastroenterite origem infecc presumível, Outras doenças infecciosas intestinais, Leptospirose icterohemorrágica, Outras formas de leptospirose, Leptospirose não especificada, Tracoma, Febre amarela, Dengue [dengue clásssico], Febre hemorrágica devida ao vírus da dengue, Restante de outr febr arbovírus febr hemor vírus, Outras hepatites virais, Micoses, Malária por Plasmodium falciparum, Malária por Plasmodium vivax, Malária por Plasmodium malariae, Outras formas malária conf exames parasitológ, Malária não especificada, Leishmaniose visceral, Leishmaniose cutânea, Leishmaniose cutâneo-mucosa, Leishmaniose não especificada, Tripanossomíase, Esquistossomose, Outras infestações por trematódeos, Equinococose, Dracunculíase, Filariose, Ancilostomíase, Outras helmintíases, Outras doenças infecciosas e parasitárias, Conjuntivite e outros transtornos da conjuntiva, Infecções da pele e do tecido subcutâneo, Outras doenças da pele e do tecido subcutâneo', '‰'),
  )
);

global $ATRIBUTOS;
$ATRIBUTOS = array(
  'Água' => array(
    'Disponibilidade' => array(
      'As instalações de água devem ser acessíveis e compatíveis com as necessidades da população no presente e no futuro, sendo que a água deve ser suficiente e contínua para usos pessoais e domésticos, incluindo água para lavagem das mãos, higiene menstrual e o manejo das fezes das crianças.',
      array(1, 5, 7)
    ),
    'Segurança' => array(
      'A água deve ser segura para o consumo humano e para a higiene pessoal e doméstica. Para isso, as instalações sanitárias devem se localizar onde a segurança física possa ser garantida, os sanitários devem estar disponíveis para serem utilizados a qualquer momento do dia ou da noite, devem ser higiênicos e, além disso, os excretas devem ser eliminados de forma segura.',
       array(1, 7, 25, 27),
    ),
    'Acessibilidade Física' => array(
      'A infraestrutura deve estar localizada de maneira genuinamente acessível - dentro, ou na proximidade imediata, do lar, local de trabalho e instituições de ensino ou de saúde - levando-se em consideração pessoas que enfrentam barreiras específicas, como crianças, idosos, pessoas com necessidades especiais e portadores de doenças crônicas.',
      array(1, 5, 7),
    ),
    'Acessibilidade Financeira' => array(
      'As pessoas possam arcar com os custos relativos ao acesso à água. Em outras palavras, o pagamento para satisfazer os diferentes usos da água não deve comprometer a capacidade das pessoas de adquirir outros bens e serviços básicos, incluindo alimentação, moradia, saúde e educação, garantidos por outros direitos humanos.',
      array(1, 4, 11, 20),
    ),
    'Igualdade e não discriminação' => array(
      'O direito de acesso à água se aplica a todos os membros da sociedade, sem discriminação por local de residência, nível de rendimentos, etnia, gênero ou outras características.',
      array(3, 4, 8, 9, 10, 11, 12, 16, 18, 20, 22),
    ),
  ),
  'Esgoto' => array(
    'Disponibilidade' => array(
      'As instalações de esgotamento sanitário devem ser acessíveis e compatíveis com as necessidades da população no presente e no futuro, sendo que a água deve ser suficiente e contínua para usos pessoais e domésticos, incluindo água para lavagem das mãos, higiene menstrual e o manejo das fezes das crianças.',
       array(1, 4, 6),
    ),
    'Segurança' => array(
      'O esgotamento sanitário deve ser seguro e adequado. Para isso, as instalações sanitárias devem se localizar onde a segurança física possa ser garantida, os sanitários devem estar disponíveis para serem utilizados a qualquer momento do dia ou da noite, devem ser higiênicos e, além disso, os excretas devem ser eliminados de forma segura.',
       array(1, 6, 38),
    ),
    'Acessibilidade Física' => array(
      'A infraestrutura deve estar localizada de maneira genuinamente acessível - dentro, ou na proximidade imediata, do lar, local de trabalho e instituições de ensino ou de saúde - com considerações a pessoas que enfrentam barreiras específicas, como crianças, idosos, pessoas com necessidades especiais e portadores de doenças.',
      array(1, 4, 6),
    ),
    'Acessibilidade Financeira' => array(
      'As pessoas devem poder arcar com os custos relativos ao acesso ao esgoto. Em outras palavras, o pagamento para assegurar o acesso ao esgotamento sanitário não deve comprometer a capacidade das pessoas de adquirir outros bens e serviços básicos, incluindo alimentação, moradia, saúde e educação, garantidos por outros direitos humanos.',
       array(1,3,10,19),
    ),
    'Igualdade e não discriminação' => array(
      'O direito de acesso ao esgotamento sanitário se aplica a todos os membros da sociedade, sem discriminação por local de residência, nível de rendimentos, etnia, gênero ou outras características.',
       array(),
    ),
  ),
);

global $MUNICIPIOS;

$MUNICIPIOS = array(
  'Baldim',
  'Belo Horizonte',
  'Betim',
  'Brumadinho',
  'Caeté',
  'Capim Branco',
  'Confins',
  'Contagem',
  'Esmeraldas',
  'Florestal',
  'Ibirité',
  'Igarapé',
  'Itaguara',
  'Itatiaiuçu',
  'Jaboticatubas',
  'Nova União',
  'Juatuba',
  'Lagoa Santa',
  'Mário Campos',
  'Mateus Leme',
  'Matozinhos',
  'Nova Lima',
  'Pedro Leopoldo',
  'Raposos',
  'Ribeirão das Neves',
  'Rio Acima',
  'Rio Manso',
  'Sabará',
  'Santa Luzia',
  'São Joaquim de Bicas',
  'São José da Lapa',
  'Sarzedo',
  'Taquaraçu de Minas',
  'Vespasiano',
);

global $CATEGORY_ID;
$CATEGORY_ID = 28;

global $MUNICIPIOS_PARENT_ID;
$MUNICIPIOS_PARENT_ID = 62727;

global $SERVICO_PARENT_ID;
$SERVICO_PARENT_ID = array(
  'Água' => 62602,
  'Esgoto'=> 62607
);

?>
