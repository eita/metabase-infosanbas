<?php
/**
 * Plugin Name: Metabase Infosanbas
 * Version: 0.0.1
 * Plugin URI: http://infosanbas.org.br
 * Description: Use metabase data to draw custom maps, tables and charts
 * Author: Alan Tygel
 * Author URI: http://www.cirandas.net/alantygel
 * Requires at least: 4.0
 * Tested up to: 4.0
 *
 * Text Domain: metabase-infosanbas
 * Domain Path: /lang/
 *
 * @package WordPress
 * @author Alan Tygel
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Load plugin class files.
require_once 'includes/class-metabase-infosanbas.php';
require_once 'includes/class-metabase-infosanbas-settings.php';

// Load plugin libraries.
require_once 'includes/lib/class-metabase-infosanbas-admin-api.php';
require_once 'includes/lib/class-metabase-infosanbas-post-type.php';
require_once 'includes/lib/class-metabase-infosanbas-taxonomy.php';

// Load configuration files.
require_once 'includes/config.php';

/**
 * Returns the main instance of Metabase_Infosanbas to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object Metabase_Infosanbas
 */
function metabase_infosanbas() {
	$instance = Metabase_Infosanbas::instance( __FILE__, '1.0.0' );

	if ( is_null( $instance->settings ) ) {
		$instance->settings = Metabase_Infosanbas_Settings::instance( $instance );
	}

	return $instance;
}

metabase_infosanbas();
