/**
 * Plugin Template frontend js.
 *
 *  @package WordPress Plugin Template/JS
 */

jQuery( document ).ready(
	function ($) {
		$("[id^=legendabtn]").click(function(){

			var indicador = $(this).attr('data-legenda')
			var modal = $("[id=legendamodal_"+indicador+"]")
			// console.log(modal)
			modal.css("display", "block");

			$(".modal-content span").click(function(e){
				modal.css("display", "none");
			});

			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {
			  if (modal.is(event.target)) {
			    modal.css("display", "none");
			  }
			}
		});
	}
);
